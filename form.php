<?php

session_start();

?>

<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
          <a class="navbar-brand" href="#">COVID-19</a>
          <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
              aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavId">
          </div>
      </nav>

      <div class="container">
        <div class="row">
              <div class="offset-2 col-md-8">
                    <img src="img/edad.jpg" class="img-fluid" alt="...">
               </div>
          <div class="offset-md-2 col-md-8">
          <blockquote class="blockquote">
            <p class="mb-0">Debes responder a las siguientes preguntas.</p>
            <footer class="blockquote-footer">Se sincero, todos lo agradeceremos, <cite title="Source Title"> Grácias!</cite></footer>
          </blockquote>
          </div>
          <div class="offset-md-2 col-md-8">
            <div class="form-group">
            <form action="form2.php" method="POST">
              <label for="edad">
                  <h4>Introduce tu edad:</h4>
              </label>
              <select class="form-control" name="edad" id="" required>
                <option value="0-18" >0-18</option>
                <option value="18-35">18-35</option>
                <option value="35-50">35-50</option>
                <option value="50-65">50-65</option>
                <option value=">65">>65</option>
              </select>
                <button type="submit" class="btn btn-primary" style="margin-top:10px;">Siguente</button>
            </form>
            </div>
          </div>
        </div>
      </div>

    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>