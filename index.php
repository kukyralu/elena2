<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
      <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
          <a class="navbar-brand" href="#">INFORMARCIÓN COVID-19</a>
          <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
              aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavId">
          </div>
      </nav>

        <div class="container" style="margin-top:20px" >
            <div class="row">
                <div class="col-md-12">
                    <img src="img/covid.jpg" class="img-fluid" alt="...">
                </div>
                <div class="col-md-12">
                <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <!--TITULO CABECERA -->  
                        ¿QUÉ ES EL COVID-19?
                        </button>
                    </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">

            <!-- AQUI TIENES QUE PONER CONTENIDO -->        
                        Some placeholder content for the first accordion panel. This panel is shown by default, thanks to the <code>.show</code> class.
            <!-- FIN CONTENIDO -->  
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <!--TITULO CABECERA -->  
                        SINTOMAS COVID-19
                        </button>
                    </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
            <!-- AQUI TIENES QUE PONER CONTENIDO -->  
                        Some placeholder content for the second accordion panel. This panel is hidden by default.
            <!-- FIN CONTENIDO -->  
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <!--TITULO CABECERA -->  
                        PRECAUCIONES
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
            <!-- AQUI TIENES QUE PONER CONTENIDO -->  
                        And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.
            <!-- FIN CONTENIDO -->  
                    </div>
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top:50px">
            <div class="card">
                <div class="card-header">
                    <h3>Qué hacer frente al Covid-19.</h3>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                    <p class="card-text">
            <!--INSERTAR CONTENIDO -->  
                    With supporting text below as a natural lead-in to additional content.</p>
            <!--FIN CONTENIDO -->  
                    <a href="<?php echo 'form.php';?>" class="btn btn-primary">Realizar Diagnóstico</a>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>