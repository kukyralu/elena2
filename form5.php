<?php
session_start();

$_SESSION['contacto_persona'] = $_POST['contacto_persona'];


?>

<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
          <a class="navbar-brand" href="#">COVID-19</a>
          <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
              aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="collapsibleNavId">
              <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                  <li class="nav-item active">
                      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                  </li>              
              </ul>
          </div>
      </nav>

      <div class="container">
        <div class="row">
        <div class="offset-2 col-md-8">
                    <img src="img/sintomas.jpg" class="img-fluid" alt="...">
               </div>
          <div class="offset-2 col-md-8">
            <div class="form-group">
            <form action="diagnostico2.php" method="POST">
                <div class="form-group">
                  <label for="">
                  <h4>
                    ¿Presentas algunos de los siguientes sintomas?
                </h4>    
                </label>
                  <select multiple class="form-control" name="sintomas[]" id="" required>
                    <option value="a"> ninguno </option>
                    <option value="b">Fiebre</option>
                    <option value="c">Tos</option>
                    <option value="d">Dolor garganta</option>
                    <option value="e">Congestion</option>
                    <option value="f">Moqueo</option>
                    <option value="g">Dolor cabeza</option>
                    <option value="h">Nauseas</option>
                    <option value="i">Perdida olfato o gusto</option>
                    <option value="j">Dificultdad respirar</option>
                    <option value="k">Dolor pecho</option>
                    <option value="l">Erupciones cutaneas</option>
                    <span class="text-right"><small>*Si desea seleccionar varios sintomas aprete Ctrl sin soltar.<small></span>
                  </select>
                </div>
                <span class="text-right"><small>*Si desea seleccionar varios sintomas aprete Ctrl sin soltar.<small></span>
                <div>
                <a href="form4.php" class="btn btn-primary">Anterior</a>
                <button type="submit" class="btn btn-primary">Siguente</button>
                </div>
                
            </form>
            </div>
          </div>
        </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
